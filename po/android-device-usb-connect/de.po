# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# anticapitalista <anticapitalista@riseup.net>, 2020
# delix02, 2020
# wahnsinn, 2020
# Vinzenz Vietzke <vinz@vinzv.de>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-22 20:45+0300\n"
"PO-Revision-Date: 2020-05-22 17:47+0000\n"
"Last-Translator: Vinzenz Vietzke <vinz@vinzv.de>, 2020\n"
"Language-Team: German (https://www.transifex.com/anticapitalista/teams/10162/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: android-device-usb-connect.sh:12
msgid "Android Device USB Connect"
msgstr "Android-Gerät USB-Anschluss"

#: android-device-usb-connect.sh:18
msgid "Clearing files and preparing directories"
msgstr "Dateien bereinigen und Verzeichnisse vorbereiten"

#: android-device-usb-connect.sh:27
msgid "Checking availability of required utilities"
msgstr "Überprüfung der Verfügbarkeit der erforderlichen Dienstprogramme"

#: android-device-usb-connect.sh:31
msgid "Error: yad is not available"
msgstr "Fehler: yad ist nicht verfügbar"

#: android-device-usb-connect.sh:35
msgid "Error: jmtpfs is not available"
msgstr "Fehler: jmtpfs ist nicht verfügbar"

#: android-device-usb-connect.sh:37
msgid "Checking availability of required utilities finished successfully"
msgstr ""
"Prüfung der Verfügbarkeit der erforderlichen Dienstprogramme erfolgreich "
"abgeschlossen"

#: android-device-usb-connect.sh:43
msgid "An android device seems to be mounted"
msgstr "Ein Android-Gerät scheint eingehängt zu sein"

#: android-device-usb-connect.sh:44
msgid ""
"An android device seems to be mounted.\\n \\nChoose 'Unmount' to unplug it "
"safely OR \\n Choose 'Access device' to view the device's contents again.   "
msgstr ""
"Ein Android-Gerät scheint eingehängt zu sein.\\n \\nWählen Sie 'Aushängen', "
"um es sicher zu trennen ODER \\n Wählen Sie 'Zugriff Gerät', um den Inhalt "
"des Geräts erneut anzuzeigen."

#: android-device-usb-connect.sh:44
msgid "Access device"
msgstr "Zugriff Gerät"

#: android-device-usb-connect.sh:44
msgid "Unmount"
msgstr "Aushängen"

#: android-device-usb-connect.sh:46
msgid "User has chosen to access the android device"
msgstr ""
"Der Benutzer hat sich für den Zugriff auf das Android-Gerät ausgewählt."

#: android-device-usb-connect.sh:47
msgid "User has chosen to unmount the android device"
msgstr ""
"Der Benutzer hat sich dafür entschieden, das Android-Gerät abzuhängen."

#: android-device-usb-connect.sh:50 android-device-usb-connect.sh:51
msgid "Android device WAS NOT umounted for some reason, do not unplug!"
msgstr ""
"Android-Gerät wurde aus irgendeinem Grund NICHT ausgehängt, trennen Sie "
"nicht den Anschuss!"

#: android-device-usb-connect.sh:51 android-device-usb-connect.sh:54
msgid "OK"
msgstr "OK"

#: android-device-usb-connect.sh:53 android-device-usb-connect.sh:54
msgid "Android device is umounted; it is safe to unplug!"
msgstr "Das Android-Gerät wurde ausgehängt; es kann sicher entfernt werden!"

#: android-device-usb-connect.sh:66
msgid "No device connected"
msgstr "Kein Gerät angeschlossen"

#: android-device-usb-connect.sh:67
msgid "Device is connected"
msgstr "Gerät ist angeschlossen"

#: android-device-usb-connect.sh:69
msgid ""
"No (MTP enabled) Android device found!\\n  \\n Connect a single device using"
" its USB cable and \\n make sure to select 'MTP' or 'File share' option and "
"retry.   \\n"
msgstr ""
"Kein (MTP-aktiviert) Android-Gerät gefunden!\\n \\n Schließen Sie ein "
"einzelnes Gerät mit seinem USB-Kabel an und \\n stellen Sie sicher, dass Sie"
" die Option 'MTP' oder 'Dateifreigabe' wählen und versuchen Sie es erneut. "
"\\n"

#: android-device-usb-connect.sh:69
msgid "EXIT"
msgstr "BEENDEN"

#: android-device-usb-connect.sh:69
msgid "Retry"
msgstr "Wiederholen"

#: android-device-usb-connect.sh:71
msgid "User pressed Exit"
msgstr "Benutzer hat Beenden ausgewählt"

#: android-device-usb-connect.sh:72
msgid "User pressed Retry"
msgstr "Benutzer hat Wiederholen ausgewählt"

#: android-device-usb-connect.sh:82
msgid "Device is mounted!"
msgstr "Gerät ist eingehängt!"

#: android-device-usb-connect.sh:84
msgid "Device is NOT mounted!"
msgstr "Gerät ist NICHT eingehängt!"

#: android-device-usb-connect.sh:86
msgid "Attempted to mount device and display its contents"
msgstr "Versucht das Gerät einzuhängen und seinen Inhalt anzuzeigen."

#: android-device-usb-connect.sh:91
msgid ""
"Checking if device can be mounted, asking user to grant permission on the "
"device and try to mount again"
msgstr ""
"Prüft, ob das Gerät eingehängt werden kann, den Benutzer um Zugriff auf das "
"Gerät bitten und erneut versuchen, es einzuhängen."

#: android-device-usb-connect.sh:93
msgid "Device seems properly mounted!"
msgstr "Gerät scheint korrekt eingehängt zu sein!"

#: android-device-usb-connect.sh:95 android-device-usb-connect.sh:100
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed"
msgstr ""
"Bitte überprüfen Sie, ob Sie den Zugriff auf Ihre Dateien auf Ihrem Android-"
"Gerät ERLAUBT haben, um fortzufahren."

#: android-device-usb-connect.sh:95
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed\\n \\n Note: If you did not allow access, simply "
"unplug, allow permission, and plug in your device's USB cable once more"
msgstr ""
"Bitte überprüfen Sie, ob Sie den Zugriff auf Ihre Dateien auf Ihrem Android-"
"Gerät ERLAUBT haben, um fortzufahren\\n \\n Hinweis: Wenn Sie den Zugriff "
"nicht erlaubt haben, ziehen Sie einfach den Netzstecker, erteilen Sie die "
"Erlaubnis und stecken Sie das USB-Kabel Ihres Geräts erneut ein"

#: android-device-usb-connect.sh:98
msgid ""
"Final check to see if device can be mounted. If not, unmount it to avoid any"
" errors"
msgstr ""
"Abschließende Prüfung, ob das Gerät eingehängt werden kann. Falls nicht, "
"entfernen Sie es, um Fehler zu vermeiden."

#: android-device-usb-connect.sh:99
msgid "The device seems to be correctly mounted."
msgstr "Das Gerät scheint korrekt eingehängt zu sein."

#: android-device-usb-connect.sh:100
msgid ""
" Unable to mount device! \\n Please check you correctly selected 'MTP...' or"
" 'File transfer...' option.\\n Or 'Allowed' file access.\\n \\n Unplug, "
"allow permission, and plug in your device and try again."
msgstr ""
" Gerät kann nicht eingehängt werden! \\n Bitte überprüfen Sie, ob Sie die "
"Option 'MTP...' oder 'Dateiübertragung...' korrekt ausgewählt haben.\\n Oder"
" 'Erlaubter' Dateizugriff.\\n \\n Stecken Sie das Gerät ab, erteilen Sie die"
" Erlaubnis, schließen Sie es an und versuchen Sie es erneut."
