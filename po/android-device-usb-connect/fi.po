# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# James Bowlin <BitJam@gmail.com>, 2020
# En Kerro <inactive+ekeimaja@transifex.com>, 2020
# Oi Suomi On! <oisuomion@protonmail.com>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-22 20:45+0300\n"
"PO-Revision-Date: 2020-05-22 17:47+0000\n"
"Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>, 2020\n"
"Language-Team: Finnish (https://www.transifex.com/anticapitalista/teams/10162/fi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fi\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: android-device-usb-connect.sh:12
msgid "Android Device USB Connect"
msgstr "Android-laitteen USB-yhdistäminen"

#: android-device-usb-connect.sh:18
msgid "Clearing files and preparing directories"
msgstr "Putsataan tiedostoja ja valmistellaan hakemistot"

#: android-device-usb-connect.sh:27
msgid "Checking availability of required utilities"
msgstr "Tarkistetaan vaadittujen apuohjelmien saatavuutta"

#: android-device-usb-connect.sh:31
msgid "Error: yad is not available"
msgstr "Virhe: yad ei ole saatavilla"

#: android-device-usb-connect.sh:35
msgid "Error: jmtpfs is not available"
msgstr "Virhe: jmtpfs ei ole saatavilla"

#: android-device-usb-connect.sh:37
msgid "Checking availability of required utilities finished successfully"
msgstr "Vaadittujen apuohjelmien tarkistus viimeistelty onnistuneesti"

#: android-device-usb-connect.sh:43
msgid "An android device seems to be mounted"
msgstr "Android-laite näyttää olevan liitettynä"

#: android-device-usb-connect.sh:44
msgid ""
"An android device seems to be mounted.\\n \\nChoose 'Unmount' to unplug it "
"safely OR \\n Choose 'Access device' to view the device's contents again.   "
msgstr ""
"Android-laite näyttää olevan liitettynä.\\n \\nValitse 'Irrota liitos' "
"irrottaaksesi sen turvallisesti TAI \\n Valitse 'Avaa laite' "
"tarkastellaksesi laitteen sisältöä uudelleen."

#: android-device-usb-connect.sh:44
msgid "Access device"
msgstr "Avaa laite"

#: android-device-usb-connect.sh:44
msgid "Unmount"
msgstr "Irrota"

#: android-device-usb-connect.sh:46
msgid "User has chosen to access the android device"
msgstr "Käyttäjä on valinnut Android-laitteen sisällön avaamisen"

#: android-device-usb-connect.sh:47
msgid "User has chosen to unmount the android device"
msgstr "Käyttäjä on valinnut Android-laitteen irroittamisen"

#: android-device-usb-connect.sh:50 android-device-usb-connect.sh:51
msgid "Android device WAS NOT umounted for some reason, do not unplug!"
msgstr ""
"Android-laitteen liitosta EI saatu irti jostakin syystä, älä irroita "
"laitetta!"

#: android-device-usb-connect.sh:51 android-device-usb-connect.sh:54
msgid "OK"
msgstr "OK"

#: android-device-usb-connect.sh:53 android-device-usb-connect.sh:54
msgid "Android device is umounted; it is safe to unplug!"
msgstr ""
"Android-laitteen liitos on katkaistu; voit nyt turvallisesti irroittaa sen! "

#: android-device-usb-connect.sh:66
msgid "No device connected"
msgstr "Ei laitetta kytkettynä"

#: android-device-usb-connect.sh:67
msgid "Device is connected"
msgstr "Laite on kytketty"

#: android-device-usb-connect.sh:69
msgid ""
"No (MTP enabled) Android device found!\\n  \\n Connect a single device using"
" its USB cable and \\n make sure to select 'MTP' or 'File share' option and "
"retry.   \\n"
msgstr ""
"Android-laitetta (MTP-mediansiirtoprotokollan kautta kytkettyä) ei "
"löytynyt!\\n \\n Yhdistä yksittäinen laite käyttäen sen USB-johtoa ja \\n "
"varmista että valitset tilaksi 'MTP' tai 'Tiedostonjako' ja yritä uudelleen."
"   \\n"

#: android-device-usb-connect.sh:69
msgid "EXIT"
msgstr "POISTU"

#: android-device-usb-connect.sh:69
msgid "Retry"
msgstr "Yritä uudelleen"

#: android-device-usb-connect.sh:71
msgid "User pressed Exit"
msgstr "Käyttäjä painoi Poistu"

#: android-device-usb-connect.sh:72
msgid "User pressed Retry"
msgstr "Käyttäjä painoi Yritä uudelleen"

#: android-device-usb-connect.sh:82
msgid "Device is mounted!"
msgstr "Laite on liitetty!"

#: android-device-usb-connect.sh:84
msgid "Device is NOT mounted!"
msgstr "Laitetta EI ole liitetty!"

#: android-device-usb-connect.sh:86
msgid "Attempted to mount device and display its contents"
msgstr "Laitteen liittämistä ja sen sisällön näyttämistä yritettiin "

#: android-device-usb-connect.sh:91
msgid ""
"Checking if device can be mounted, asking user to grant permission on the "
"device and try to mount again"
msgstr ""
"Tarkistetaan josko laite voidaan liittää, käyttäjältä kysytään valtuutusta "
"laitteen liittämisen uudelleenyritykselle"

#: android-device-usb-connect.sh:93
msgid "Device seems properly mounted!"
msgstr "Laita näyttää olevan oikein liitetty!"

#: android-device-usb-connect.sh:95 android-device-usb-connect.sh:100
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed"
msgstr ""
"Tarkista että olet antanut luvan Android-laitteesi tiedostojen käsittelyyn "
"jatkaaksesi"

#: android-device-usb-connect.sh:95
msgid ""
"Please check that you have ALLOWED access to your files on your android "
"device in order to proceed\\n \\n Note: If you did not allow access, simply "
"unplug, allow permission, and plug in your device's USB cable once more"
msgstr ""
"Tarkista että olet antanut luvan Android-laitteesi tiedostojen käsittelyyn "
"jatkaaksesi\\n \\n Huomautus: mikäli et valtuuttanut lupaa laitteen tietojen"
" käsittelyyn, irroita laite, anna tarvittava lupa, ja liitä laitteesi USB-"
"johto jälleen kerran"

#: android-device-usb-connect.sh:98
msgid ""
"Final check to see if device can be mounted. If not, unmount it to avoid any"
" errors"
msgstr ""
"Viimeinen tarkistus voidaanko laite liittää. Mikäli näin ei ole, irroita "
"laiteliitos välttääksesi virheitä"

#: android-device-usb-connect.sh:99
msgid "The device seems to be correctly mounted."
msgstr "Tämä laite näyttää olevan liitetty oikein."

#: android-device-usb-connect.sh:100
msgid ""
" Unable to mount device! \\n Please check you correctly selected 'MTP...' or"
" 'File transfer...' option.\\n Or 'Allowed' file access.\\n \\n Unplug, "
"allow permission, and plug in your device and try again."
msgstr ""
"Laitetta ei voitu liittää! \\n Tarkista että valitsit oikein 'MTP...' tai "
"'Tiedostonjako...\" vaihtoehdon.\\n Tai 'Sallit' luvan tiedostojen "
"käsittelyyn.\\n \\n Irroita, salli lupa, ja liitä laitteesi johdolla "
"uudestaan."
