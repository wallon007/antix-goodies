# Translations for unplugdrive.sh package (Ver.0.82a).
# Copyright (C) 2021 THE unplugdrive'S COPYRIGHT HOLDER
# This file is distributed under the same license as the unplugdrive package.
# 
# Translators:
# Automatically generated, 2021
# cyril cottet <cyrilusber2001@yahoo.fr>, 2017,2019
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011
# Jonathan Chevalier <jonathan.chevalier@icloud.com>, 2016
# sombreros <sebastiendebierre@gmail.com>, 2011
# Wallon Wallon, 2021
# Wallon Wallon, 2021
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-30 10:23+0200\n"
"PO-Revision-Date: 2021-06-03 19:22+0000\n"
"Last-Translator: Wallon Wallon\n"
"Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: unplugdrive.sh:33
msgid "Unplug USB device"
msgstr "Débrancher le périphérique USB"

#: unplugdrive.sh:35
msgid "Version "
msgstr "Version "

#: unplugdrive.sh:68
msgid "  Unplugdrive"
msgstr "  Débrancher le périphérique"

#: unplugdrive.sh:70
msgid "  GUI tool for safely unplugging removable USB devices."
msgstr "  Outil GUI pour débrancher en toute sécurité les\\n  périphériques USB amovibles."

#: unplugdrive.sh:72
msgid "  Usage: unplugdrive.sh [options]"
msgstr "  Utilisation : unplugdrive.sh [options]"

#: unplugdrive.sh:74
msgid "  Options:"
msgstr "  Options :"

#: unplugdrive.sh:75
msgid "\\t-h or --help\\t\\tdisplays this help text"
msgstr "\\t-h ou --help\\t\\taffiche ce texte d'aide"

#: unplugdrive.sh:76
msgid ""
"\\t-s or --safe\\t\\truns script in safe mode,\\n\\t            "
"\\t\\tdisplaying some extra dialogs"
msgstr "\\t-s or --safe\\t\\texécute le script en mode sans échec,\\n\\t            \\t\\taffichage de quelques boîtes de dialogue supplémentaires"

#: unplugdrive.sh:77
msgid "\\t-d or --decorated\\tuses window decorations"
msgstr "\\t-d ou --decorated\\tutilise les décorations de fenêtre"

#: unplugdrive.sh:78
msgid "\\t-c or --centred\\t\\topen dialogs centred"
msgstr "\\t-c or --centred\\t\\tdialogues ouverts centrés"

#: unplugdrive.sh:79
msgid "\\t-g or --debug\\t\\tenable debug output (terminal)"
msgstr "\\t-g ou --debug\\t\\tactiver la sortie de débogage (terminal)"

#: unplugdrive.sh:80
msgid ""
"\\t-p or --pretend\\t\\tdry run, don't actually un-\\n\\t            "
"\\t\\tmount drives (for debugging)"
msgstr "\\t-p ou --pretend\\t\\tun essai, ne le fait pas réellement un-\\n\\t            \\t\\tmonter les périphériques (pour débogage)"

#: unplugdrive.sh:82
msgid "  Questions, Suggestions and Bugreporting please to:"
msgstr "  Questions, suggestions et rapport de bug s'il vous plaît à :"

#: unplugdrive.sh:113
msgid ""
"Invalid command line argument. Please call\\nunplugdrive -h or --help for "
"usage instructions."
msgstr "Argument de ligne de commande invalide. Veuillez consulter\\nunplugdrive -h or --help pour les instructions d'utilisation."

#: unplugdrive.sh:122
msgid ""
"\\nˮyadˮ is not installed.\\n   --> Please install ˮyadˮ before executing "
"this script.\\n"
msgstr "\\nˮyadˮ n'est pas installé.\\n   --> S'il vous plaît, installer ˮyadˮ avant d'exécuter ce script.\\n"

#: unplugdrive.sh:123
msgid "Taskbar icon $icon_taskbar not found."
msgstr "L'icône de la barre des tâches $icon_taskbar est introuvable."

#: unplugdrive.sh:139
msgid "<b>Aborting</b> on user request\\n<b>without unmounting.</b>"
msgstr "<b>Abandonner</b> à la demande de l'utilisateur\\n<b>sans démontage.</b>"

#: unplugdrive.sh:197
msgid ""
"A removable drive with a mounted\\npartition was not found.\\n<b>It is safe "
"to unplug the drive(s)</b>"
msgstr "Un disque amovible avec une partition\\nmontée n'a pas été trouvé.\\n<b>Vous pouvez débrancher le(s) périphérique(s) en toute sécurité</b>"

#: unplugdrive.sh:211
msgid ""
"<b>Mounted USB partitions:</b>\\n$removablelist<b>Choose the drive(s) to be "
"unplugged:</b>"
msgstr "<b>Partitions USB montées :</b>\\n$removablelist<b>Choisissez le(s) lecteur(s) à débrancher :</b>"

#: unplugdrive.sh:257
msgid ""
"<b>About to unmount:</b>\\n$summarylist<b>Please confirm you wish to "
"proceed.</b>"
msgstr "<b>Prêt à être démonté :</b>\\n$summarylist<b>Veuillez confirmer que vous souhaitez poursuivre.</b>"

#: unplugdrive.sh:266
msgid "Data is being written\\nto devices. <b>Please wait...</b>"
msgstr "Des données sont en train d'être\\nécrites sur des périphériques. <b>S'il vous plaît, patientez...</b>"

#: unplugdrive.sh:326
msgid ""
"\\033[1;31mWARNING: DEVICE ${deviceslist[u]} WAS NOT PROPERLY "
"UNMOUNTED!\\033[0m\\n\\033[4;37mPlease check before unplugging.\\033[0m"
msgstr "\\033[1;31mATTENTION : Le périphérique ${deviceslist[u]} n'a pas été démonté proprement !\\033[0m\\n\\033[4;37mVeuillez vérifier avant de débrancher.\\033[0m"

#: unplugdrive.sh:345
msgid ""
"<b>Mountpoint removal failed.</b>\\n<u>One or more mountpoin(s) remain "
"present at:</u>"
msgstr "<b>La suppression du point de montage a échoué.</b>\\n<u>Un ou plusieurs point(s) de montage reste(nt) présents préventif :</u>"

#: unplugdrive.sh:345
msgid "<b>Check each mountpoint listed before unpluging the drive(s).</b>"
msgstr "<b>Vérifiez chaque point de montage indiqué avant de débrancher le(s) périphérique(s).</b>"

#: unplugdrive.sh:353
msgid ""
"<b>Unmounted:</b>\\n$summarylist\\n<b>It is safe to unplug the drive(s)</b>"
msgstr "<b>Démonté :</b>\\n$summarylist\\n<b>Vous pouvez débrancher le(s) disque(s) en toute sécurité</b>"
