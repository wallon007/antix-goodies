��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    >   �     �  %   �  0     1   C     u     y     �     �  M   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2021
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Zgjidhni Zonë Kohore (duke përdorur tastet kursor dhe Enter) Datë: Administroni Rregullime Date dhe Kohe Që të saktësoni Orën, lëvizni rrëshqitesin Që të saktoni Minutën, lëvizni rrëshqitësin Dil Përzgjidhni Zonë Kohore Caktoni Datën e Tanishme Caktoni Kohën e Tanishme Përdorni shërbyes Kohe Internetore, që koha/data të ujdisen automatikisht 