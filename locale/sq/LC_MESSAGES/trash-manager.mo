��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  7   �       T        o     �  a   �  2   �  '   &     N  0   m  "  �           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2023
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Jeni i sigurt se doni të zbrazet koshi i hedhurinave?   U bë   të hapësirës tuaj në disk.\n Hapësira e lirë në dosjen tuaj shtëpi është: Zbraz Hedhurinat Zbraz hedhurinat Gabim. Diç shkoi ters. \n Ju lutemi, kontrolloni se keni dhënë një përzgjedhje të vlefshme. Riktheni dorazi një kartelë që prej hedhurinave Përzgjidhni kartelë që të shfshihet Përgjegjës koshi hedhurinash \n Koshi juaj i hedhurina aktualisht po përdor  \n \n Shënim: mund të përdorni përgjegjësin tuaj të kartelave për të tërhequr thjesht kartelë(a), ose dosje prej Koshit të Hedhurinave e drejt e atje ku i doni,\n ose bëjeni dorazi, duke përzgjedhur numrin e tyre prej një liste, duke përdorur butonin '$empty_trash_button' \n 