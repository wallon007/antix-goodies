��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �  	   W     a     m  Z     X   �     3     <     [  +   c  M   �  �   �      �  6   �  7   �  1      0   R                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Appliquer Précédent Modifier les noms Cliquez sur Appliquer lorsque les noms des espaces de travail souhaités ont été saisis. Cliquez sur Terminé lorsque le nombre d’espaces de travail souhaité a été défini. Terminé Saisissez les noms souhaités: Quitter Définissez le nombre souhaité de bureaux: Pour modifier les noms des espaces de travail, cliquez sur Modifier les noms. Utilisez la molette de la souris ou cliquez sur les flèches. Vous pouvez également saisir un numéro au clavier suivi de la touche Entrée. Le changement a lieu immédiatement. Numéro de l’espace de travail Sélecteur de nombre d’espaces de travail aWCS antiX Changement de nom de l’espace de travail - aWCS antiX Sélecteur de nombre d’espaces de travail antiX Changement de nom de l’espace de travail antiX 