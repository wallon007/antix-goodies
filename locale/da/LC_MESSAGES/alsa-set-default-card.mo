��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  |  �  1     5   G     }     �      �     �     �     �               &      4     U                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: scootergrisen, 2018
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
 (kunne ikke oprette forbindelse til valgte enhed) (kunne ikke oprette forbindelse til den valgte enhed) Nuværende standard er %s Fandt ingen lydkort/-enheder Der blev kun fundet ét lydkort. Vælg venligst lydkort Afslut Lydkort indstillet til %s Lydtest Test mislykkedes Test lykkedes Tester lyden i op til 6 sekunder Vil du teste om lyden virker? 