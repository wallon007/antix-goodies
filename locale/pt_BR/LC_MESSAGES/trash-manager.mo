��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  �        �  a   �     *     =  L   P  4   �  $   �     �  )     #  8           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://app.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
   Você tem certeza que quer esvaziar a lixeira?
  Se você esvaziar a lixeira, todos os arquivos
  e pastas serão excluídos permanentemente. Foi concluído com sucesso  de espaço do seu dispositivo de armazenamento.\n O espaço disponível na sua pasta pessoal é: Esvaziar a Lixeira Esvaziar a Lixeira Ocorreu um erro. \n Por favor, verifique se você fez uma seleção válida. Restaurar manualmente um ou mais arquivos da lixeira Selecione o arquivo a ser recuperado Gerenciador da Lixeira \n A lixeira está utilizando no momento  \n \n Observação: Você pode utilizar o gerenciador de arquivos para simplesmente arrastar o(s) arquivo(s) ou a(s) pasta(s)\n da Lixeira para um outro local onde desejar, ou fazê-lo selecionando manualmente o respectivo número\n da lista, utilizando o botão ‘$empty_trash_button’ \n 