��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  !   *  !   L     n     �     �     �     �     �     �     �             '   &                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: OkayPJ <1535253694@qq.com>, 2021
Language-Team: Chinese (China) (http://www.transifex.com/anticapitalista/antix-development/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 （可以连接到选中设备） （无法连接到选中设备） 现在默认是 %s 没有找到声卡/声音设备 只找到一个声卡。 请选择声卡 退出 声卡已设置为%s 声音测试 测试失败 测试成功 测试声音持续 6 秒 您想要测试声音是否工作吗？ 