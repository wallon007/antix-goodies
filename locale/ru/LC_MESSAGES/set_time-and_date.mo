��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    �        �  D   �  @   �  F   6  
   }  (   �  .   �  2   �  �                          	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Andrei Stepanov, 2022
Language-Team: Russian (https://www.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Выберите часовой пояс (с помощью клавиш управления курсором и клавиши ввода) Число: Управление установок числа и времени Переместите ползунок на нужный час Переместите ползунок на нужную минуту Выход Выберите часовой пояс Установка текущего числа Установка текущего времени Использование интернет-сервера для автоматической установки времени и числа 