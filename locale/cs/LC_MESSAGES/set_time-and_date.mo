��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    =   �     #     *  %   D  %   j     �     �     �     �  F   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: joyinko <joyinko@azet.sk>, 2021
Language-Team: Czech (https://www.transifex.com/anticapitalista/teams/10162/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
 Vyberte časové pásmo (použitím kurzoru a klávesy Enter) Datum: Nastavení datumu a času Přesuňte jezdec na správnou Hodinu Přesuňte jezdec na správnou Minutu Konec Vybrat Časové pásmo Nastavit dnešní datum Nastavit aktuální čas Automaické nastavení datumu a času použítím Internetového času 