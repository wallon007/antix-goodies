��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    =   �          $  #   @  %   d     �     �     �     �  R   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Escolher a Zona Horária (usando as teclas do cursor e Enter) Data: Definições de Data e Hora Mover o cursor para a Hora correcta Mover o cursor para o Minuto correcto Sair Seleccionar a Zona Horária Introduzir a Data Actual Introduzir a Hora Actual Usar o servidor de 'Hora da Internet' para definir automaticamente a data e a hora 