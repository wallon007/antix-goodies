��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  1   d  6   �     �  2   �  '        F     _     d     �     �     �     �     �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: José Vieira <jvieira33@sapo.pt>, 2016,2019
Language-Team: Portuguese (http://www.transifex.com/anticapitalista/antix-development/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 (Foi possível ligar ao dispositivo seleccionado) (Não foi possível ligar ao dispositivo seleccionado) A predefinição actual é %s Não foram encontrados placas/dispositivos de som. Foi encontrada apenas uma placa de som. Seleccionar placa de som Sair Placa de som definida para %s Teste de som O teste falhou O teste teve êxito A testar o som até 6 segundos Verificar se o som funciona? 