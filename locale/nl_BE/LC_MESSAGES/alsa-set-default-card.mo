��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  5   (  :   ^     �  /   �  $   �           '     ,     I     U     b  +   n  "   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Vanhoorne Michael, 2022
Language-Team: Dutch (Belgium) (http://www.transifex.com/anticapitalista/antix-development/language/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
 (Kon verbinding maken met het geselecteerde apparaat) (Kon geen verbinding maken met het geselecteerde apparaat) Huidige standaard is %s Er zijn geen geluidskaarten/-apparaten gevonden Er is maar een geluidskaart gevonden Selecteer a.u.b. de geluidskaart Stop Geluidskaart ingesteld op %s Geluid test Test mislukt Test gelukt Geluid testen gedurende maximaal 6 seconden Wilt u testen of het geluid werkt? 