��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  3   W  9   �     �  /   �  &     !   :     \     a  
   ~     �     �  -   �  2   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2019
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 (Si potrebbe connettere il dispositivo selezionato) (Non è possibile connettersi al dispositivo selezionato) L'impostazione corrente è %s Non sono stati trovati schede/dispositivi audio E' stata trovata solo una scheda audio Prego, seleziona una scheda audio Esci Scheda audio impostata su %s Test audio Test fallito Il test ha avuto successo L'audio verrà testato per massimo 6 secondi  Vuoi verificare se l'audio funziona correttamente? 