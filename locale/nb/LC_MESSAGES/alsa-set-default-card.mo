��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  (   3  -   \     �  !   �     �     �     �     �                    +     I                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2020
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 (Klarte å koble til den valgte enheten) (Klarte ikke å koble til den valgte enheten) Gjeldende forvalg er %s Fant ingen lydkort eller -enheter Fant kun ett lydkort. Velg lydkort Avslutt Bruker %s som lydkort Lydtest Mislykket test Vellykket test Tester lyd opp til 6 sekunder Vil du teste om lyden virker? 