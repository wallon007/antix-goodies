��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m    �  2   �  7   �  .     .   4  '   c  !   �  
   �  *   �     �     �       0   !  $   R                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-29 21:26+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2021
Language-Team: Hebrew (Israel) (http://www.transifex.com/anticapitalista/antix-development/language/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 (ניתן להתחבר אל ההתקן הנבחר) (לא ניתן להתחבר אל ההתקן הנבחר) ברירת המחדל הנוכחית היא %s לא נמצאו התקני/כרטיסי שמע רק כרטיס שמע אחד נמצא. נא לבחור כרטיס שמע עזיבה כרטיס הקול הוגדר בתור %s בדיקת שמע הבדיקה כשלה הבדיקה צלחה מתבצעת בדיקת שמע עד 6 שניות שנבדוק אם השמע עובד? 