��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    O        _  /   k  B   �  B   �  
   !      ,  $   M  "   r  f   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>, 2022
Language-Team: Hebrew (Israel) (https://www.transifex.com/anticapitalista/teams/10162/he_IL/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: he_IL
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n == 2 && n % 1 == 0) ? 1: (n % 10 == 0 && n % 1 == 0 && n > 10) ? 2 : 3;
 נא לבחור את אזור הזמן (באמצעות הסמן ומקש enter) תאריך: ניהול הגדרות התאריך והשעה יש להזיז את מחוון הגלילה לשעה הנכונה יש להזיז את מחוון הגלילה לדקה הנכונה יציאה נא לבחור אזור זמן. הגדרת התאריך הנוכחי הגדרת השעה הנוכחית להשתמש בשרת זמן מהאינטרנט כדי להגדיר שעה/תאריך אוטומטית 