��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  o   �     L  �   e  )   �          ,  p   �  H     <   f  l   �  �             	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Είστε βέβαιοι ότι θέλετε να αδειάσετε τον κάδο απορριμμάτων; Ολοκληρώθηκε του χώρου στο δίσκο σας.\n Ο ελεύθερος χώρος στον αρχικό σας φάκελο είναι: Άδειασμα απορριμμάτων Αδειος κάδος Λάθος. Κάτι πήγε στραβά. \n Ελέγξτε εάν έχετε εισαγάγει έγκυρη επιλογή. Μη αυτόματη επαναφορά ενός αρχείου από τον κάδο απορριμμάτων Επιλέξτε αρχείο για αναίρεση διαγραφής Διαχειριστής κάδου απορριμμάτων \n Ο κάδος απορριμμάτων σας χρησιμοποιείται αυτήν τη στιγμή \n \n Σημείωση: μπορείτε να χρησιμοποιήσετε τη διαχείριση αρχείων σας για να σύρετε απλώς αρχεία ή φακέλους από τον Κάδο απορριμμάτων πίσω στο σημείο που θέλετε,\n ή να το κάνετε επιλέγοντας χειροκίνητα τον αριθμό τους από μια λίστα, χρησιμοποιώντας το κουμπί '$empty_trash_button' \n 