��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     Z     b     h  [   x  Q   �     &      ,     M  -   S  R   �  �   �     �  9   �  6   �  4     2   E                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Aplicar Atras Cambiar nombres Pulse en Aplicar cuando haya introducido los nombres deseados para los espacios de trabajo. Pulse en Listo cuando haya configurado el número deseado de espacios de trabajo. Hecho Introduzca los nombres deseados: Salir Establecer el número de escritorios deseado: Para cambiar los nombres de los espacios de trabajo, haga clic en Cambiar nombres. Utilice la rueda del ratón o pulse en las flechas. También puede introducir un número mediante el teclado seguido de la tecla intro. El cambio se produce inmediatamente. Número de espacio de trabajo Controlador de recuento de espacios de trabajo aWCS antiX Cambiador de nombres de espacios de trabajo antiX aWCS Controlador de recuento de espacios de trabajo antiX cambiador de nombre de espacio de trabajo de antiX 