��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �      >   4     s  $   y  %   �  $   �     �     �          !  L   5                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: MX Linux Polska <mxlinuxpl@gmail.com>, 2022
Language-Team: Polish (https://www.transifex.com/anticapitalista/teams/10162/pl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pl
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 Wybierz strefę czasową (za pomocą klawiszy kursora i enter) Data: Zarządzaj ustawieniami daty i czasu Przesuń suwak do właściwej godziny Przesuń suwak do właściwej minuty Zakończ Wybierz strefę czasową Ustaw aktualną datę Ustaw aktualny czas Użyj internetowego serwera czasu, aby automatycznie ustawić godzinę/datę 