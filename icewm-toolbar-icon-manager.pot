# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-19 16:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: icewm-toolbar-icon-manager:13
msgid "You are running an IceWM desktop"
msgstr ""

#: icewm-toolbar-icon-manager:15 icewm-toolbar-icon-manager:121
msgid "Warning"
msgstr ""

#: icewm-toolbar-icon-manager:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr ""

#: icewm-toolbar-icon-manager:77 icewm-toolbar-icon-manager:86
#: icewm-toolbar-icon-manager:102 icewm-toolbar-icon-manager:135
#: icewm-toolbar-icon-manager:155 icewm-toolbar-icon-manager:335
msgid "Toolbar Icon Manager"
msgstr ""

#: icewm-toolbar-icon-manager:77
msgid "Help::TXT"
msgstr ""

#: icewm-toolbar-icon-manager:77
msgid ""
"What is this?\\nThis utility adds and removes application icons to IceWm's "
"toolbar.\\nThe toolbar application icons are created from an application's ."
"desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is "
"created during an application's installation process to allow the system "
"easy access to relevant information, such as the app's full name, commands "
"to be executed, icon to be used, where it should be placed in the OS menu, "
"etc.\\nA .desktop file name usually refers to the app's name, which makes it "
"very easy to find the intended .desktop file (ex: Firefox ESR's .desktop "
"file is 'firefox-esr.desktop').\\nWhen adding a new icon to the toolbar, the "
"user can click the field presented in the main window and a list of all the ."
"desktop files of the installed applications will be shown.\\nThat, in fact, "
"is a list of (almost) all installed applications that can be added to the "
"toolbar.\\nNote: some of antiX's applications are found in the sub-folder "
"'antiX'.\\n\n"
"TIM buttons:\\n 'ADD ICON' - select, from the list, the .desktop file of the "
"application you want to add to your toolbar and it instantly shows up on the "
"toolbar.\\nIf, for some reason, TIM fails to find the correct icon for your "
"application, it will still create a toolbar icon using the default 'gears' "
"image so that you can still click to access the application.\\nYou can click "
"the 'Advanced' button to manually edit the relevant entry and change the "
"application's icon.\\n'UNDO LAST STEP' - every time an icon is added or "
"removed from the toolbar, TIM creates a backup file. If you click this "
"button, the toolbar is instantly restored from that backup file, without any "
"confirmation.\\n'REMOVE ICON' - this shows a list of all applications that "
"have icons on the toolbar. Double left click any application to remove its "
"icon from the toolbar\\n'MOVE ICON' - this shows a list of all applications "
"that have icons on the toolbar. Double left click any application to select "
"it and then move it to the left or to the right\\n'ADVANCED' - allows for "
"editing the text configuration file that has all of your desktop's toolbar "
"icon's configurations. Manually editing this file allows the user to "
"rearrange the order of the icons and delete or add any icon. A brief "
"explanation about the inner workings of the text configuration file is "
"displayed before the file is opened for editing.\\n Warnings: only manually "
"edit a configuration file if you are sure of what you are doing! Always make "
"a back up copy before editing a configuration file!"
msgstr ""

#: icewm-toolbar-icon-manager:86
msgid "Warning::TXT"
msgstr ""

#: icewm-toolbar-icon-manager:86
msgid ""
"If you click 'Yes', the toolbar configuration file will be opened for manual "
"editing.\\n\n"
"How-to:\\nEach toolbar icon is identified by a line starting with 'prog' "
"followed by the application name, icon and the application executable file."
"\\n Move, edit or delete the entire line referring to each toolbar icon "
"entry.\\nNote: Lines starting with # are comments only and will be ignored."
"\\nThere can be empty lines.\\nSave any changes and then restart IceWM."
"\\nYou can undo the last change from TIMs UNDO LAST STEP button."
msgstr ""

#: icewm-toolbar-icon-manager:87
msgid "Ok"
msgstr ""

#: icewm-toolbar-icon-manager:102
msgid "Double click any Application to remove its icon:"
msgstr ""

#: icewm-toolbar-icon-manager:102
msgid "Remove"
msgstr ""

#: icewm-toolbar-icon-manager:113
msgid "file has something"
msgstr ""

#: icewm-toolbar-icon-manager:119
msgid "file is empty"
msgstr ""

#: icewm-toolbar-icon-manager:121
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""

#: icewm-toolbar-icon-manager:135
msgid "Double click any Application to move its icon:"
msgstr ""

#: icewm-toolbar-icon-manager:135
msgid "Move"
msgstr ""

#: icewm-toolbar-icon-manager:145
msgid "nothing was selected"
msgstr ""

#: icewm-toolbar-icon-manager:155
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr ""

#: icewm-toolbar-icon-manager:157
msgid "Move left"
msgstr ""

#: icewm-toolbar-icon-manager:158
msgid "Move right"
msgstr ""

#: icewm-toolbar-icon-manager:223
msgid "Add selected app's icon"
msgstr ""

#: icewm-toolbar-icon-manager:223
msgid "Choose application to add to the Toolbar"
msgstr ""

#: icewm-toolbar-icon-manager:339
msgid "HELP!help:FBTN"
msgstr ""

#: icewm-toolbar-icon-manager:340
msgid "ADVANCED!help-hint:FBTN"
msgstr ""

#: icewm-toolbar-icon-manager:341
msgid "ADD ICON!add:FBTN"
msgstr ""

#: icewm-toolbar-icon-manager:342
msgid "REMOVE ICON!remove:FBTN"
msgstr ""

#: icewm-toolbar-icon-manager:343
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr ""

#: icewm-toolbar-icon-manager:344
msgid "UNDO LAST STEP!undo:FBTN"
msgstr ""

#: icewm-toolbar-icon-manager:345
msgid "Please select any option from the buttons below to manage Toolbar icons"
msgstr ""
